Création automatisée de commandes
=================================

But
---

Pour un projet de document à la mise en page très spéciales, je cherche à créer
un grand nombre de commande pouvant être redéfinie avec un contenu assez
complexe, sans ré écrire l'entier des commandes.
